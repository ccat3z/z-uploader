package com.c0ldcat.zuploader;

import com.c0ldcat.zuploader.data.Base;
import com.c0ldcat.zuploader.data.FileBase;
import com.c0ldcat.zuploader.health.DataBaseHealth;
import com.c0ldcat.zuploader.health.DataPathHealth;
import com.c0ldcat.zuploader.resources.PersonResource;
import com.c0ldcat.zuploader.resources.TaskResource;
import com.c0ldcat.zuploader.resources.UploadFileResource;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.io.File;
import java.util.EnumSet;

public class ZUploaderApplication extends Application<ZUploaderConfiguration> {
    Logger logger = LoggerFactory.getLogger(this.getClass());

    public static void main(final String[] args) throws Exception {
        new ZUploaderApplication().run(args);
    }

    @Override
    public String getName() {
        return "ZUploader";
    }

    @Override
    public void initialize(final Bootstrap<ZUploaderConfiguration> bootstrap) {
    }

    @Override
    public void run(final ZUploaderConfiguration configuration,
                    final Environment environment) {
        configureCors(environment);

        Base base;
        try {
            base = new ObjectMapper().readerFor(Base.class).readValue(new File(configuration.getDataConfig()));
        } catch (Exception e) {
            base = new Base();
        }
        base.setPath(configuration.getDataConfig());

        File dataDir = new File(configuration.getDataPath());
        if (dataDir.exists()) {
            logger.info("Data directory exist.");
        } else {
            logger.info("Not found data directory, creating it...");
            dataDir.mkdirs();
        }
        FileBase fb = new FileBase(configuration.getDataPath());

        environment.jersey().register(MultiPartFeature.class);
        environment.jersey().register(new UploadFileResource(configuration.getDataPath()));
        environment.jersey().register(new PersonResource(base));
        environment.jersey().register(new TaskResource(base, fb));

        environment.healthChecks().register("database", new DataBaseHealth(base));
        environment.healthChecks().register("data-path", new DataPathHealth(configuration.getDataPath()));
    }

    static private void configureCors(Environment environment) {
        final FilterRegistration.Dynamic cors =
                environment.servlets().addFilter("CORS", CrossOriginFilter.class);

        // Configure CORS parameters
        cors.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
        cors.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM, "X-Requested-With,Content-Type,Accept,Origin,Authorization");
        cors.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "OPTIONS,GET,PUT,POST,DELETE,HEAD");
        cors.setInitParameter(CrossOriginFilter.ALLOW_CREDENTIALS_PARAM, "true");

        // Add URL mapping
        cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
    }
}
