package com.c0ldcat.zuploader.resources;

import com.c0ldcat.zuploader.api.data.person.*;
import com.c0ldcat.zuploader.data.Base;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/person")
@Produces(MediaType.APPLICATION_JSON)
public class PersonResource {
    private Base db;

    public PersonResource(Base db) {
        this.db = db;
    }

    @GET
    public ListPerson listPerson() {
        return new ListPerson(db);
    }

    @GET
    @Path("/{uuid}/add")
    public AddPerson addPerson(@QueryParam("name") String name,
                               @QueryParam("passwd") String passwd,
                               @PathParam("uuid") String uuid) {
        AddPerson addPerson = new AddPerson(db, name, passwd, uuid);
        db.save();
        return addPerson;
    }

    @GET
    @Path("/{uuid}/del")
    public DelPerson delPerson(@PathParam("uuid") String uuid,
                               @QueryParam("passwd") String passwd) {
        DelPerson delPerson = new DelPerson(db, uuid, passwd);
        db.save();
        return delPerson;
    }

    @GET
    @Path("/{uuid}/new-passwd")
    public ChangePasswd changePasswd(@PathParam("uuid") String uuid,
                                     @QueryParam("passwd") String passwd,
                                     @QueryParam("new-passwd") String newPasswd) {
        ChangePasswd changePasswd = new ChangePasswd(db, uuid, passwd, newPasswd);
        db.save();
        return changePasswd;
    }

    @GET
    @Path("{uuid}/check")
    public Check check(@PathParam("uuid") String uuid,
                       @QueryParam("passwd") String passwd) {
        Check check = new Check(db, uuid, passwd);
        db.save();
        return check;
    }

    @GET
    @Path("/{uuid}/task")
    public ListTask listTask(@PathParam("uuid") String uuid) {
        return new ListTask(db, uuid);
    }

    @GET
    @Path("/{uuid}/leader-task")
    public ListTaskAsLeader listTaskAsLeader(@PathParam("uuid") String uuid) {
        return new ListTaskAsLeader(db, uuid);
    }
}
