package com.c0ldcat.zuploader.resources;

import com.c0ldcat.zuploader.api.file.GetFile;
import com.c0ldcat.zuploader.api.file.UploadFile;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import javax.ws.rs.*;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;

@Path("/uploader")
@Produces(MediaType.APPLICATION_JSON)
public class UploadFileResource {
    private String dataPath;

    public UploadFileResource(String dataPath) {
        this.dataPath = dataPath;
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public UploadFile upload(@FormDataParam("file")InputStream is,
                             @FormDataParam("file")FormDataContentDisposition formDataContentDisposition) {
        return new UploadFile(dataPath, formDataContentDisposition.getFileName(), is);
    }

    @GET
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response download(@QueryParam("file")String file){
        System.out.println(file);
        try {
            if (file == null) {
                throw new FileNotFoundException();
            }
            InputStream is = GetFile.get(dataPath, file);
            return Response.ok(is).header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + file + "\"").build();
        } catch (FileNotFoundException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @Path("/multi-get")
    public Response multiGet(HashMap<String, String> fileMap) {
        return Response.ok(GetFile.getFilesTar(dataPath, fileMap)).build();
    }
}
