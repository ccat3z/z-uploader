package com.c0ldcat.zuploader.resources;

import com.c0ldcat.zuploader.api.data.task.*;
import com.c0ldcat.zuploader.api.file.GetFile;
import com.c0ldcat.zuploader.data.Base;
import com.c0ldcat.zuploader.data.FileBase;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import javax.ws.rs.*;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

@Path("/task")
@Produces(MediaType.APPLICATION_JSON)
public class TaskResource {
    private Base db;
    private FileBase fb;

    public TaskResource(Base db, FileBase fb) {
        this.db = db;
        this.fb = fb;
    }

    @GET
    @Path("/{uuid}/add")
    public AddTask addTask(@QueryParam("leader") String leaderUuid,
                           @QueryParam("passwd") String leaderPasswd,
                           @QueryParam("description") String description,
                           @PathParam("uuid") String uuid) {
         AddTask addTask = new AddTask(db, leaderUuid, leaderPasswd, description, uuid);
         db.save();
         return addTask;
    }

    @GET
    @Path("/{uuid}/del")
    public DelTask delTask(@QueryParam("leader") String leaderUuid,
                           @QueryParam("passwd") String leaderPasswd,
                           @PathParam("uuid") String uuid) {
        DelTask delTask = new DelTask(db, leaderUuid, leaderPasswd, uuid);
        db.save();
        return delTask;
    }

    @GET
    @Path("/{uuid}/add-person")
    public AddPerson addPerson(@QueryParam("leader") String leaderUuid,
                               @QueryParam("passwd") String leaderPasswd,
                               @PathParam("uuid") String uuid,
                               @QueryParam("user") String userUuid) {
        AddPerson addPerson = new AddPerson(db, leaderUuid, leaderPasswd, uuid, userUuid);
        db.save();
        return addPerson;
    }

    @GET
    @Path("/{uuid}/del-person")
    public DelPerson delPerson(@QueryParam("leader") String leaderUuid,
                               @QueryParam("passwd") String leaderPasswd,
                               @PathParam("uuid") String uuid,
                               @QueryParam("user") String userUuid) {
        DelPerson delPerson = new DelPerson(db, leaderUuid, leaderPasswd, uuid, userUuid);
        db.save();
        return delPerson;
    }

    @GET
    @Path("/{uuid}")
    public Response getTask(@PathParam("uuid") String uuid) {
        try {
            return Response.ok(db.getTask(uuid)).build();
        } catch (Base.TaskNotFoundException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @GET
    @Path("/{uuid}/get")
    @Produces("application/tar")
    public Response download(@PathParam("uuid") String uuid) {
        try {
            try {
                return Response.ok(GetFile.getFilesTar(fb.getDataPath(), db.getTask(uuid).getTarMap(db.getPersonList()))).header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + URLEncoder.encode(uuid, "UTF-8") + ".tar\"").build();
            } catch (UnsupportedEncodingException e) {
                return Response.ok(GetFile.getFilesTar(fb.getDataPath(), db.getTask(uuid).getTarMap(db.getPersonList()))).header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"output.tar\"").build();
            }
        } catch (Base.TaskNotFoundException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @GET
    public ListTask getTaskList() {
        return new ListTask(db);
    }

    @POST
    @Path("/{uuid}/uploader")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Upload upload(@FormDataParam("file")InputStream is,
                         @FormDataParam("file")FormDataContentDisposition formDataContentDisposition,
                         @QueryParam("uuid") String uuid,
                         @QueryParam("passwd") String passwd,
                         @PathParam("uuid") String taskUuid) {
        Upload upload = new Upload(fb, db, uuid, passwd, taskUuid, formDataContentDisposition.getFileName(), is);
        db.save();
        return upload;
    }
}
