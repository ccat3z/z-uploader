package com.c0ldcat.zuploader;

import io.dropwizard.Configuration;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ZUploaderConfiguration extends Configuration {
    private String dataPath;
    private String dataConfig;

    @JsonProperty
    public void setDataPath(String dataPath) {
        this.dataPath = dataPath;
    }

    public String getDataPath() {
        return dataPath;
    }

    @JsonProperty
    public void setDataConfig(String dataConfig) {
        this.dataConfig = dataConfig;
    }

    public String getDataConfig() {
        return dataConfig;
    }
}
