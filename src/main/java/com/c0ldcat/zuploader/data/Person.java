package com.c0ldcat.zuploader.data;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Person {
    private String name;
    private String passwd;
    private String uuid;

    @JsonCreator
    public Person(@JsonProperty("name") String name,
                  @JsonProperty("passwd") String passwd,
                  @JsonProperty("uuid") String uuid) {
        this.name = name;
        this.passwd = passwd;
        this.uuid = uuid;
    }

    @JsonProperty
    public String getName() {
        return name;
    }

    @JsonProperty
    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    @JsonProperty
    public String getUuid() {
        return uuid;
    }

    @Override
    public boolean equals(Object o) {
        return uuid.equals(((Person) o).uuid);
    }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }
}
