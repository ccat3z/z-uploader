package com.c0ldcat.zuploader.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;

public class Base {
    private HashSet<Person> personSet = new HashSet<>();
    private HashSet<Task> taskSet = new HashSet<>();
    @JsonIgnore
    private String path;

    public Base() {

    }

    public Base(String path) {
        this.path = path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public boolean addPerson(Person person) {
        return personSet.add(person);
    }

    public boolean delPerson(Person person) {
        return personSet.remove(person);
    }

    public Person getPerson(String uuid) throws PersonNotFoundException {
        for (Person person : personSet) {
            if (person.getUuid().equals(uuid))
                return person;
        }
        throw new PersonNotFoundException();
    }

    public boolean addTask(Task task) {
        return taskSet.add(task);
    }

    public boolean delTask(Task task) {
        return taskSet.remove(task);
    }

    public Task getTask(String uuid) throws TaskNotFoundException {
        for (Task task : taskSet) {
            if (task.getUuid().equals(uuid))
                return task;
        }
        throw new TaskNotFoundException();
    }

    @JsonProperty("persons")
    public HashSet<Person> getPersonSet() {
        return personSet;
    }

    @JsonProperty("persons")
    public void setPersonSet(HashSet<Person> personSet) {
        this.personSet = personSet;
    }

    @JsonProperty("tasks")
    public HashSet<Task> getTaskSet() {
        return taskSet;
    }

    @JsonProperty("tasks")
    public void setTaskSet(HashSet<Task> taskSet) {
        this.taskSet = taskSet;
    }

    @JsonIgnore
    public HashMap<String, String> getTaskList() {
        HashMap<String, String> result = new HashMap<>();
        for (Task task : taskSet) {
            result.put(task.getUuid(), task.getDescription());
        }
        return result;
    }

    @JsonIgnore
    public HashMap<String, String> getPersonList() {
        HashMap<String, String> result = new HashMap<>();
        for (Person person : personSet) {
            result.put(person.getUuid(), person.getName());
        }
        return result;
    }

    public HashMap<String, String> getTaskListByLeaderUuid(String uuid) {
        HashMap<String, String> result = new HashMap<>();
        for (Task task : taskSet) {
            if (task.getLeaderUuid().equals(uuid)) {
                result.put(task.getUuid(), task.getDescription());
            }
        }
        return result;
    }

    public HashMap<String, String> getTaskListByUserUuid(String uuid) {
        HashMap<String, String> result = new HashMap<>();
        for (Task task : taskSet) {
            if (task.getFiles().containsKey(uuid)) {
                result.put(task.getUuid(), task.getDescription());
            }
        }
        return result;
    }

    public void save() {
        if (path != null) {
            File f = new File(path);
            try {
                new ObjectMapper().writerWithDefaultPrettyPrinter().writeValue(f, this);
            } catch (IOException e) {
                //ignore it
            }
        }
    }

    public class PersonNotFoundException extends Exception {
        //pass
    }

    public class TaskNotFoundException extends Exception {
        //pass
    }
}
