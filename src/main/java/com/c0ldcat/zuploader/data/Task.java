package com.c0ldcat.zuploader.data;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.HashSet;

public class Task {
    private String leaderUuid;
    private String description;
    private String uuid;

    private HashMap<String, HashSet<String>> files;

    @JsonCreator
    public Task(@JsonProperty("leader") String leaderUuid,
                @JsonProperty("description") String description,
                @JsonProperty("uuid") String uuid,
                @JsonProperty("files") HashMap<String, HashSet<String>> files) {
        this.leaderUuid = leaderUuid;
        this.description = description;
        this.uuid = uuid;
        this.files = files;
    }

    public Task(Person person, String description, String uuid) {
        this.leaderUuid = person.getUuid();
        this.description = description;
        this.uuid = uuid;
        this.files = new HashMap<>();
    }

    public Task(String leaderUuid, String description, String uuid) {
        this.leaderUuid = leaderUuid;
        this.description = description;
        this.uuid = uuid;
        this.files = new HashMap<>();
    }

    @JsonProperty("leader")
    public String getLeaderUuid() {
        return leaderUuid;
    }

    @JsonProperty
    public String getDescription() {
        return description;
    }

    @JsonProperty
    public String getUuid() {
        return uuid;
    }

    @JsonProperty
    public HashMap<String, HashSet<String>> getFiles() {
        return files;
    }

    @JsonIgnore
    public HashMap<String, String> getTarMap(HashMap<String, String> personMap) {
        HashMap<String, String> result = new HashMap<>();
        for (String person : files.keySet()) {
            for (String file : files.get(person)) {
                result.put(file, new StringBuilder().append(personMap.get(person)).append('/').append(file).toString());
            }
        }

        return result;
    }

    public boolean addPerson(Person person) {
        if (!files.containsKey(person.getUuid())) {
            files.put(person.getUuid(), new HashSet<String>());
            return true;
        } else {
            return false;
        }
    }

    public boolean delPerson(Person person) {
        if (files.containsKey(person.getUuid())) {
            files.remove(person.getUuid());
            return true;
        } else {
            return false;
        }
    }

    public boolean delPerson(String uuid) {
        if (files.containsKey(uuid)) {
            files.remove(uuid);
            return true;
        } else {
            return false;
        }
    }

    public boolean pushFile(FileBase fb, String uuid, String fileUuid) throws FileNotFoundException{
        if (files.containsKey(uuid)) {
            HashSet f = files.get(uuid);
            if (fb.check(fileUuid)) {
                return f.add(fileUuid);
            } else {
                throw new FileNotFoundException();
            }
        } else {
            return false;
        }
    }

    @Override
    public boolean equals(Object o) {
        return uuid.equals(((Task) o).uuid);
    }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }
}
