package com.c0ldcat.zuploader.data;

import java.io.File;

public class FileBase {
    private String dataPath;

    public FileBase(String dataPath) {
        this.dataPath = dataPath;
    }

    public boolean check(String uuid) {
        return new File(dataPath + "/" + uuid).exists();
    }

    public String getDataPath() {
        return dataPath;
    }
}
