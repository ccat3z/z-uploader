package com.c0ldcat.zuploader.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NormalApi {
    private boolean success = false;
    private String message;

    @JsonProperty
    public boolean getSuccess() {
        return success;
    }

    @JsonProperty
    public String getMessage() {
        return message;
    }

    protected void setSuccess(boolean success) {
        this.success = success;
    }

    protected void setMessage(String message) {
        this.message = message;
    }
}
