package com.c0ldcat.zuploader.api.file;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.*;
import java.util.UUID;

public class UploadFile {
    private boolean success = false;
    private String fileName;

    public UploadFile(String dataDir, String postFileName, InputStream is) {
        fileName = new StringBuilder().append(UUID.randomUUID().toString()).append('-').append(postFileName).toString();

        try {
            writeToFile(is, dataDir + "/" + fileName);
            success = true;
        } catch (IOException e) {
            fileName = null;
            success = false;
        }
    }

    @JsonProperty
    public String getFileName() {
        return fileName;
    }

    @JsonProperty
    public boolean getSuccess() {
        return success;
    }

    private void writeToFile(InputStream uploadedInputStream, String uploadedFileLocation) throws IOException {
        int read;
        final int BUFFER_LENGTH = 1024;
        final byte[] buffer = new byte[BUFFER_LENGTH];
        OutputStream out = new FileOutputStream(new File(uploadedFileLocation));
        while ((read = uploadedInputStream.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
        out.flush();
        out.close();
    }
}
