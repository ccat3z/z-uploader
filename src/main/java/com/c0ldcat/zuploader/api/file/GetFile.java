package com.c0ldcat.zuploader.api.file;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.io.IOUtils;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;
import java.io.*;
import java.util.HashMap;

public class GetFile {
   static public InputStream get(String dataPath, String file) throws FileNotFoundException {
       File f = new File(dataPath + "/" + file);
       return new FileInputStream(f);
   }

   static public StreamingOutput getFilesTar(final String dataPath, final HashMap<String, String> filesMap) {
       return new StreamingOutput() {
           @Override
           public void write(OutputStream outputStream) throws IOException, WebApplicationException {
               TarArchiveOutputStream tarArchiveOutputStream = new TarArchiveOutputStream(outputStream);

               tarArchiveOutputStream.setLongFileMode(TarArchiveOutputStream.LONGFILE_POSIX);

               for (String fileName : filesMap.keySet()) {
                   File file = new File(dataPath + '/' + fileName);
                   if (file.exists()) {
                       InputStream is = new FileInputStream(file);

                       TarArchiveEntry tarArchiveEntry = new TarArchiveEntry(file, filesMap.get(fileName));

                       tarArchiveOutputStream.putArchiveEntry(tarArchiveEntry);

                       IOUtils.copy(is, tarArchiveOutputStream);
                       tarArchiveOutputStream.closeArchiveEntry();

                       is.close();
                       System.out.println(fileName);
                   }
               }

               tarArchiveOutputStream.finish();
               tarArchiveOutputStream.close();
           }
       };
    }
}
