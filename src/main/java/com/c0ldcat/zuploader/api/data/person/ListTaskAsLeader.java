package com.c0ldcat.zuploader.api.data.person;

import com.c0ldcat.zuploader.data.Base;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;

@JsonPropertyOrder({"tasks"})
public class ListTaskAsLeader {
    public HashMap<String, String> tasks;

    public ListTaskAsLeader (Base db, String uuid) {
        tasks = db.getTaskListByLeaderUuid(uuid);
    }
}
