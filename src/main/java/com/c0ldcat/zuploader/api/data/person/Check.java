package com.c0ldcat.zuploader.api.data.person;

import com.c0ldcat.zuploader.api.NormalApi;
import com.c0ldcat.zuploader.data.Base;
import com.c0ldcat.zuploader.data.Person;

public class Check extends NormalApi {
    public Check(Base db, String uuid, String passwd) {
        try {
            Person person;
            if (PersonHelper.check(db, uuid, passwd)) {
                setSuccess(true);
                setMessage("Right.");
            } else {
                setSuccess(false);
                setMessage("Password is wrong.");
            }
        } catch (Base.PersonNotFoundException e) {
            setSuccess(false);
            setMessage("User not found.");
        }
    }
}
