package com.c0ldcat.zuploader.api.data.task;

import com.c0ldcat.zuploader.api.NormalApi;
import com.c0ldcat.zuploader.data.Base;
import com.c0ldcat.zuploader.data.Task;

public class AddTask extends NormalApi {
    public AddTask(Base db, String leaderUuid, String leaderPasswd, String description, String uuid) {
        if (leaderUuid != null && leaderPasswd != null
                && description != null && uuid != null) {
            try {
                if (TaskHelper.checkLeader(db, leaderUuid, leaderPasswd)) {
                    setSuccess(db.addTask(new Task(leaderUuid, description, uuid)));
                    setMessage(getSuccess() ? "Task created." : "Task existed");
                }
            } catch (Base.PersonNotFoundException e) {
                setMessage("No such person");
            }
        } else {
            setMessage("Wrong parameters.");
        }
    }
}
