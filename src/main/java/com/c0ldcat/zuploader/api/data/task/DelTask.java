package com.c0ldcat.zuploader.api.data.task;

import com.c0ldcat.zuploader.api.NormalApi;
import com.c0ldcat.zuploader.data.Base;

public class DelTask extends NormalApi {
    public DelTask(Base db, String leaderUuid, String leaderPasswd, String uuid) {
        if (leaderUuid != null && leaderPasswd != null
                && uuid != null) {
            try {
                if (TaskHelper.checkLeader(db, leaderUuid, leaderPasswd)) {
                    try {
                        setSuccess(db.delTask(db.getTask(uuid)));
                        setMessage("Task deleted.");
                    } catch (Base.TaskNotFoundException e) {
                        setMessage("Task not found.");
                    }
                } else {
                    setMessage("Wrong password.");
                }
            } catch (Base.PersonNotFoundException e) {
                setMessage("No such person");
            }
        } else {
            setMessage("Wrong parameters.");
        }
    }
}
