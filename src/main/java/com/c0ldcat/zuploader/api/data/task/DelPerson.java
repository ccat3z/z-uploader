package com.c0ldcat.zuploader.api.data.task;

import com.c0ldcat.zuploader.api.NormalApi;
import com.c0ldcat.zuploader.data.Base;

public class DelPerson extends NormalApi{
    public DelPerson (Base db, String leaderUuid, String leaderPasswd, String uuid, String userUuid) {
        if (leaderUuid != null && leaderPasswd != null && uuid != null && userUuid != null) {
            try {
                if (TaskHelper.checkLeader(db, leaderUuid, leaderPasswd)) {
                    try {
                        setSuccess(db.getTask(uuid).delPerson(userUuid));
                        setMessage(getSuccess() ? "Deleted person from task." : "Person doesn't exist in task.");
                    } catch (Base.TaskNotFoundException e) {
                        setMessage("No such task.");
                    }
                } else {
                    setMessage("Wrong password.");
                }
            } catch (Base.PersonNotFoundException e) {
                setMessage("No such person.");
            }
        } else {
            setMessage("Wrong parameters.");
        }
    }
}
