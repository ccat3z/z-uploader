package com.c0ldcat.zuploader.api.data.person;

import com.c0ldcat.zuploader.data.Base;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;

@JsonPropertyOrder({"persons"})
public class ListPerson {
    public HashMap<String, String> persons;

    public ListPerson(Base db) {
        persons = db.getPersonList();
    }
}
