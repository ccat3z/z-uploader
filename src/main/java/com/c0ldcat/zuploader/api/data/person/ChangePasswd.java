package com.c0ldcat.zuploader.api.data.person;

import com.c0ldcat.zuploader.api.NormalApi;
import com.c0ldcat.zuploader.data.Base;
import com.c0ldcat.zuploader.data.Person;

public class ChangePasswd extends NormalApi {
    public ChangePasswd(Base db, String uuid, String passwd, String newPasswd) {
        try {
            Person person;
            if ((person = db.getPerson(uuid)).getPasswd().equals(passwd)) {
                person.setPasswd(newPasswd);
                setSuccess(true);
                setMessage("Password changed.");
            } else {
                setMessage("Password is wrong.");
            }
        } catch (Base.PersonNotFoundException e) {
            setMessage("User not found.");
        }
    }
}
