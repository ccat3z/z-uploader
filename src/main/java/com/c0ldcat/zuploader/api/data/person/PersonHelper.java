package com.c0ldcat.zuploader.api.data.person;

import com.c0ldcat.zuploader.data.Base;
import com.c0ldcat.zuploader.data.Person;

public class PersonHelper {
    static boolean check(Base db, String uuid, String passwd) throws Base.PersonNotFoundException{
        Person person = db.getPerson(uuid);
        return person.getPasswd().equals(passwd);
    }
}
