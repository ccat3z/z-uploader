package com.c0ldcat.zuploader.api.data.person;

import com.c0ldcat.zuploader.api.NormalApi;
import com.c0ldcat.zuploader.data.Base;
import com.c0ldcat.zuploader.data.Person;

public class DelPerson extends NormalApi{
    public DelPerson(Base db, String uuid, String passwd) {
        try {
            Person person;
            if ((person = db.getPerson(uuid)).getPasswd().equals(passwd)) {
                setSuccess(db.delPerson(person));
                setMessage("Deleted user.");
            } else {
                setSuccess(false);
                setMessage("Password is wrong.");
            }
        } catch (Base.PersonNotFoundException e) {
            setSuccess(false);
            setMessage("User not found.");
        }
    }
}
