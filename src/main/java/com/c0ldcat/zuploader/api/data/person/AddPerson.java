package com.c0ldcat.zuploader.api.data.person;

import com.c0ldcat.zuploader.api.NormalApi;
import com.c0ldcat.zuploader.data.Base;
import com.c0ldcat.zuploader.data.Person;

public class AddPerson extends NormalApi{
    public AddPerson(Base base, String name, String passwd, String uuid) {
        if (name != null && passwd != null && uuid != null) {
            setSuccess(base.addPerson(new Person(name, passwd, uuid)));
            setMessage(getSuccess() ? "The user " + uuid + " created." : "This person existed.");
        } else {
            setMessage("Wrong parameters.");
        }
    }
}
