package com.c0ldcat.zuploader.api.data.task;

import com.c0ldcat.zuploader.api.NormalApi;
import com.c0ldcat.zuploader.data.Base;

public class AddPerson extends NormalApi{
    public AddPerson (Base db, String leaderUuid, String leaderPasswd, String uuid, String userUuid) {
        if (leaderUuid != null && leaderPasswd != null && uuid != null && userUuid != null) {
            try {
                if (TaskHelper.checkLeader(db, leaderUuid, leaderPasswd)) {
                    try {
                        setSuccess(db.getTask(uuid).addPerson(db.getPerson(userUuid)));
                        setMessage(getSuccess() ? "Added person into task." : "Person existed.");
                    } catch (Base.PersonNotFoundException e) {
                        setMessage("No such user.");
                    } catch (Base.TaskNotFoundException e) {
                        setMessage("No such task.");
                    }
                } else {
                    setMessage("Wrong password.");
                }
            } catch (Base.PersonNotFoundException e) {
                setMessage("No such person.");
            }
        } else {
            setMessage("Wrong parameters.");
        }
    }
}
