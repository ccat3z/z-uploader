package com.c0ldcat.zuploader.api.data.task;

import com.c0ldcat.zuploader.data.Base;
import com.c0ldcat.zuploader.data.Person;

public class TaskHelper {
    static boolean checkLeader(Base db, String leaderUuid, String leaderPasswd) throws Base.PersonNotFoundException{
        Person person = db.getPerson(leaderUuid);
        return person.getPasswd().equals(leaderPasswd);
    }
}
