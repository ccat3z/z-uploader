package com.c0ldcat.zuploader.api.data.person;

import com.c0ldcat.zuploader.data.Base;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;

@JsonPropertyOrder({"tasks"})
public class ListTask {
    public HashMap<String, String> tasks;

    public ListTask (Base db, String uuid) {
        tasks = db.getTaskListByUserUuid(uuid);
    }
}
