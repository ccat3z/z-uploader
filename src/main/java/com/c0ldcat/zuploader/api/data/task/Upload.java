package com.c0ldcat.zuploader.api.data.task;

import com.c0ldcat.zuploader.api.NormalApi;
import com.c0ldcat.zuploader.api.file.UploadFile;
import com.c0ldcat.zuploader.data.Base;
import com.c0ldcat.zuploader.data.FileBase;

import java.io.FileNotFoundException;
import java.io.InputStream;

public class Upload extends NormalApi {
    public String fileName = "null";

    public Upload (FileBase fb, Base db, String uuid, String passwd, String taskUuid, String postFileName, InputStream is) {
        if (uuid != null && passwd != null && taskUuid != null ) {
            try {
                if (TaskHelper.checkLeader(db, uuid, passwd)) {
                    try {
                        UploadFile uploadFile = new UploadFile(fb.getDataPath(), postFileName, is);
                        String fileUuid = uploadFile.getFileName();
                        if (uploadFile.getSuccess()) {
                            fileName = fileUuid;
                            setSuccess(db.getTask(taskUuid).pushFile(fb, uuid, fileUuid));
                            setMessage(getSuccess() ? "Added file into task." : "No such person  existed in task.");
                        } else {
                            setMessage("Upload file failed");
                        }
                    } catch (FileNotFoundException e) {
                        setMessage("No such file on server.");
                    } catch (Base.TaskNotFoundException e) {
                        setMessage("No such task.");
                    }
                } else {
                    setMessage("Wrong password.");
                }
            } catch (Base.PersonNotFoundException e) {
                setMessage("No such person.");
            }
        } else {
            setMessage("Wrong parameters.");
        }
    }
}
