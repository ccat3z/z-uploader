package com.c0ldcat.zuploader.health;

import com.c0ldcat.zuploader.data.Base;
import com.codahale.metrics.health.HealthCheck;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DataBaseHealth extends HealthCheck{
    private Base db;

    public DataBaseHealth (Base db) {
        this.db = db;
    }
    @Override
    protected Result check() throws Exception {
        if (db != null) {
            StringBuilder sb = new StringBuilder();
            sb.append(db.getPath());
            sb.append("\n");
            sb.append(new ObjectMapper().writeValueAsString(db));
            return Result.healthy(sb.toString());
        } else {
            return Result.unhealthy("Database is null.");
        }
    }
}
