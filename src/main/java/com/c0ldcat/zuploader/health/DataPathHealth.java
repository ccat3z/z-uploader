package com.c0ldcat.zuploader.health;

import com.codahale.metrics.health.HealthCheck;

import java.io.File;

public class DataPathHealth extends HealthCheck {
    private String dataPath;

    public DataPathHealth(String dataPath) {
        this.dataPath = dataPath;
    }

    @Override
    protected Result check() throws Exception {
        File dir = new File(dataPath);
        if (! dir.exists()) {
            return Result.unhealthy("Data not exists");
        }
        return Result.healthy();
    }
}
